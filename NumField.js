"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _connectField = _interopRequireDefault(require("uniforms/connectField"));

var _filterDOMProps = _interopRequireDefault(require("uniforms/filterDOMProps"));

var noneIfNaN = function noneIfNaN(x) {
  return isNaN(x) ? undefined : x;
};

var Num_ = function Num_(_ref) {
  var className = _ref.className,
      decimal = _ref.decimal,
      disabled = _ref.disabled,
      error = _ref.error,
      errorMessage = _ref.errorMessage,
      icon = _ref.icon,
      iconLeft = _ref.iconLeft,
      iconProps = _ref.iconProps,
      id = _ref.id,
      inputRef = _ref.inputRef,
      label = _ref.label,
      max = _ref.max,
      min = _ref.min,
      name = _ref.name,
      onChange = _ref.onChange,
      placeholder = _ref.placeholder,
      required = _ref.required,
      showInlineError = _ref.showInlineError,
      step = _ref.step,
      value = _ref.value,
      wrapClassName = _ref.wrapClassName,
      props = (0, _objectWithoutProperties2.default)(_ref, ["className", "decimal", "disabled", "error", "errorMessage", "icon", "iconLeft", "iconProps", "id", "inputRef", "label", "max", "min", "name", "onChange", "placeholder", "required", "showInlineError", "step", "value", "wrapClassName"]);
  return _react.default.createElement("div", (0, _extends2.default)({
    className: (0, _classnames.default)(className, {
      disabled: disabled,
      error: error,
      required: required
    }, 'field')
  }, (0, _filterDOMProps.default)(props)), label && _react.default.createElement("label", {
    htmlFor: id
  }, label), _react.default.createElement("div", {
    className: (0, _classnames.default)('ui', wrapClassName, {
      left: iconLeft,
      icon: icon || iconLeft
    }, 'input')
  }, _react.default.createElement("input", {
    disabled: disabled,
    id: id,
    max: max,
    min: min,
    name: name,
    onChange: onChange,
    placeholder: placeholder,
    ref: inputRef,
    step: step || (decimal ? 0.01 : 1),
    type: "number",
    value: value
  }), (icon || iconLeft) && _react.default.createElement("i", (0, _extends2.default)({
    className: "".concat(icon || iconLeft, " icon")
  }, iconProps))), !!(error && showInlineError) && _react.default.createElement("div", {
    className: "ui red basic pointing label"
  }, errorMessage));
}; // NOTE: React < 16 workaround. Make it optional?


var Num =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2.default)(Num, _Component);

  function Num() {
    var _this;

    (0, _classCallCheck2.default)(this, Num);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(Num).apply(this, arguments));
    _this.state = {
      value: '' + _this.props.value
    };
    _this.onChange = _this.onChange.bind((0, _assertThisInitialized2.default)((0, _assertThisInitialized2.default)(_this)));
    return _this;
  }

  (0, _createClass2.default)(Num, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(_ref2) {
      var decimal = _ref2.decimal,
          value = _ref2.value;
      var parse = decimal ? parseFloat : parseInt;

      if (noneIfNaN(parse(value)) !== noneIfNaN(parse(this.state.value.replace(/[.,]+$/, '')))) {
        this.setState({
          value: value === undefined || value === '' ? '' : '' + value
        });
      }
    }
  }, {
    key: "onChange",
    value: function onChange(_ref3) {
      var value = _ref3.target.value;
      var change = value.replace(/[^\d.,-]/g, '');
      this.setState({
        value: change
      });
      this.props.onChange(noneIfNaN((this.props.decimal ? parseFloat : parseInt)(change)));
    }
  }, {
    key: "render",
    value: function render() {
      return Num_((0, _objectSpread2.default)({}, this.props, {
        onChange: this.onChange,
        value: this.state.value
      }));
    }
  }]);
  return Num;
}(_react.Component);

var _default = (0, _connectField.default)(Num);

exports.default = _default;