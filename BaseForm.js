"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime/helpers/objectSpread"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _get2 = _interopRequireDefault(require("@babel/runtime/helpers/get"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _BaseForm = _interopRequireDefault(require("uniforms/BaseForm"));

var _classnames = _interopRequireDefault(require("classnames"));

var Semantic = function Semantic(parent) {
  var _class, _temp;

  return _temp = _class =
  /*#__PURE__*/
  function (_parent) {
    (0, _inherits2.default)(_class, _parent);

    function _class() {
      (0, _classCallCheck2.default)(this, _class);
      return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(_class).apply(this, arguments));
    }

    (0, _createClass2.default)(_class, [{
      key: "getNativeFormProps",
      value: function getNativeFormProps() {
        var props = (0, _get2.default)((0, _getPrototypeOf2.default)(_class.prototype), "getNativeFormProps", this).call(this);
        var error = this.getChildContextError();
        return (0, _objectSpread2.default)({}, props, {
          className: (0, _classnames.default)('ui', props.className, {
            error: error
          }, 'form')
        });
      }
    }]);
    return _class;
  }(parent), _class.Semantic = Semantic, _class.displayName = "Semantic".concat(parent.displayName), _temp;
};

var _default = Semantic(_BaseForm.default);

exports.default = _default;